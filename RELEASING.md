# Releasing

Current release process is to increment the version (semantic versioning) by tagging it with next version number via git and pushing it to master. The the gitlab build system will detect a new tag version and will produce new binaries for both mac and linux and release them in the gitlab releases tab for this project.

Also don't forget to increment the version in the package.json with the new version in a commit before tagging.


## Git tag and push
Example

```shell
# tag
git tag 1.3.0 -a -m "1.3.0"
# push tag to origin
git push origin --tags
```
