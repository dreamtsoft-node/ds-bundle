# Dreamtsoft bundle-sync cli tool

## Commands

```shell
// Initialize bundle locally
ds-bundle init --spaceName DevSpace --bundleId ds.work --baseUrl https://local.dreamtsoft.com:8080 --username dev --password password

// Deletes current bundle files in the current directory then pull down all files from space configured for the current directory path
ds-bundle pull

// Push bundle files from space configured for the current directory path
ds-bundle push
// or push all files at a specific path only
ds-bundle push -p bundle_components/page/my_example_component


// Watch ds-bundle project for bundle file changes then create/update/delete on space configured
ds-bundle watch
```


## Devopment

Start dev server that compiles files

```shell
npm run dev
```

Test with the following command

```shell
chmod +x build/cli.js && ./build/cli.js $INSERT_C0MMAND
```

## Build

### Build for macos

```shell

npm run build && npm run package:macos && chmod +x bin/macos/ds-bundle

```
TODO:
Publish bin/ds-bundle binary to mac ports


### Build for linux

```shell

npm run build && npm run package:linux && chmod +x bin/linux/ds-bundle

```
TODO:
Publish bin/ds-bundle binary

## Releasing

[Release docs](RELEASING.md)
