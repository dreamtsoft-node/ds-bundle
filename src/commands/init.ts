import type { Arguments, CommandBuilder } from 'yargs';
import fs from 'fs';
import { CONFIG_FILE_NAME, DS_AUTH_FILE_NAME, CURRENT_DIR } from '../constants';
import { loadConfig } from '../lib/utils/config';
import { readFilesDSFS } from '../lib/dsfs/read';
import { cleanFilesFolder } from '../lib/utils/cleanFilesFolder';
import { log, logError } from '../lib/utils/logger';

type Options = {
  //api: string;
  spaceName: string;
  bundleId: string;
  baseUrl: string;
  username: string;
  password: string;
};


export const command: string = 'init'; // <spaceName> <bundleId> <baseUrl> <username> <password>
export const desc: string = `Initialize ${CONFIG_FILE_NAME} ${DS_AUTH_FILE_NAME} and pull initial files`;

export const builder: CommandBuilder<Options, Options> = (yargs) =>
  yargs
    .options({
      spaceName: { type: 'string', demandOption: true },
      bundleId: { type: 'string', demandOption: true },
      baseUrl: { type: 'string', demandOption: true },
      username: { type: 'string', demandOption: true },
      password: { type: 'string', demandOption: true },
    });
    /*
    .positional('spaceName', { type: 'string', demandOption: true })
    .positional('bundleId', { type: 'string', demandOption: true })
    .positional('baseUrl', { type: 'string', demandOption: true })
    .positional('username', { type: 'string', demandOption: true })
    .positional('password', { type: 'string', demandOption: true });
    */

export const handler = async (argv: Arguments<Options>): Promise<void> => {
  const { spaceName, bundleId, baseUrl, username, password } = argv;

  const configObject = {
    space: spaceName,
    bundle: bundleId,
    baseUrl,
    ignore: ["node_modules"],
  };

  log('Initializing...');

  if (fs.existsSync(`./${CONFIG_FILE_NAME}`)) {
    log(`${CONFIG_FILE_NAME} already exist don't create`);
  } else {
    fs.writeFileSync(`./${CONFIG_FILE_NAME}`, JSON.stringify(configObject, null, 2));
    log(`Created file ${CONFIG_FILE_NAME}`);
  }

  const authConfigObject = {
    username,
    password,
  };

  if (fs.existsSync(`./${DS_AUTH_FILE_NAME}`)) {
    log(`${DS_AUTH_FILE_NAME} already exist don't create`);
  } else {
    fs.writeFileSync(`./${DS_AUTH_FILE_NAME}`, JSON.stringify(authConfigObject, null, 2));
    log(`Created file ${DS_AUTH_FILE_NAME}`);
  }

  const config = loadConfig();

  if (!config) {
    logError(`${CONFIG_FILE_NAME} not found`);
    return;
  }

  log('Cleaning DS folders...');

  cleanFilesFolder(config);

  log('Pulling files...');

  try {

    await readFilesDSFS(config, CURRENT_DIR);
    log('Config initialized and files pulled');
    process.exit(0);

  } catch (err) {
    if (err instanceof Error && err.message.includes('Invalid credentials')) {
      logError(err);
      log('');
      log(`Edit ${DS_AUTH_FILE_NAME} and fix your credentials. Then try running ds-bundle pull`);
    } else {
      logError(err);
    }
    process.exit(1);
  }

};
