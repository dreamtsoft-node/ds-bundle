import type { Arguments, CommandBuilder } from 'yargs';
import { loadConfig } from '../lib/utils/config';
import { readFilesDSFS } from '../lib/dsfs/read';
import { cleanFilesFolder } from '../lib/utils/cleanFilesFolder';
import { CURRENT_DIR, CONFIG_FILE_NAME } from '../constants';
import { log, logError } from '../lib/utils/logger';

type Options = {};

export const command: string = 'pull';
export const desc: string = 'Pull all bundle files from ds instance';

export const builder: CommandBuilder<Options, Options> = (yargs) => yargs;

export const handler = async (argv: Arguments<Options>): Promise<void> => {
  const config = loadConfig();

  if (!config) {
    logError(`${CONFIG_FILE_NAME} not found`);
    return;
  }

  log('Cleaning DS folders...');

  cleanFilesFolder(config);

  const targetPath = config.targetPathRelativeToRoot ? config.targetPathRelativeToRoot : undefined;

  log('Pulling files...');

  try {

    await readFilesDSFS(config, config.rootPath, targetPath);
    log('Files pulled');
    process.exit(0);

  } catch (err) {
    logError('Failed to pull files');
    logError(err);
    process.exit(1);
  }


};
