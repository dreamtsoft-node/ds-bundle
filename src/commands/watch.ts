import type { Arguments, CommandBuilder } from 'yargs';
import { CURRENT_DIR,DS_FOLDERS, CONFIG_FILE_NAME } from '../constants';
import chokidar from 'chokidar';
import { watchFiles } from '../lib/dsfs/watch';
import { loadConfig } from '../lib/utils/config';
import { log, logError } from '../lib/utils/logger';

type Options = {};

export const command: string = 'watch';
export const desc: string = 'Watch file for changes and push to ds instance';

export const builder: CommandBuilder<Options, Options> = (yargs) =>
  yargs;

export const handler = async (argv: Arguments<Options>): Promise<void> => {
  const {} = argv;
  const dirs = DS_FOLDERS.map((dir) => CURRENT_DIR + dir);

  const watcher = chokidar.watch(dirs, {
    ignored: /(^|[\/\\])\../, // ignore dotfiles
    persistent: true,
    ignoreInitial: true,
  });

  const config = loadConfig();

  if (!config) {
    logError(`${CONFIG_FILE_NAME} not found`);
    return;
  }

  try {

    await watchFiles(config, watcher, config.rootPath);
    log('Watching files...');

  } catch (err) {
    logError('Error watching files');
    logError(err);
    process.exit(1);
  }

};
