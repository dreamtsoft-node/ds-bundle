import type { Arguments, CommandBuilder } from 'yargs';
import { CURRENT_DIR, CONFIG_FILE_NAME } from '../constants';
import { writeFilesDSFS, deleteFilesDSFS } from '../lib/dsfs/write';
import { listFilesDSFS } from '../lib/dsfs/read';
import { arrayDiff } from '../lib/utils/helpers';
import { loadConfig, cleanTargetPath } from '../lib/utils/config';
import { log, logError } from '../lib/utils/logger';

type Options = {
  path: string | undefined;
};

export const command: string = 'push';
export const desc: string = 'Push all bundle files to ds instance';

export const builder: CommandBuilder<Options, Options> = (yargs) =>
  yargs.options({
    path: { type: 'string', alias: 'p', },
  });

export const handler = async (argv: Arguments<Options>): Promise<void> => {
  const { path } = argv;

  const config = loadConfig();

  if (!config) {
    logError(`${CONFIG_FILE_NAME} not found`);
    return;
  }

  const targetPath = path ? cleanTargetPath(path) : config.targetPathRelativeToRoot;

  try {
    const currentFiles = await listFilesDSFS(config, config.rootPath, targetPath);

    log('Pushing files...');
    const filesUpdated = await writeFilesDSFS(config, config.rootPath, targetPath);
    log('Files pushed');

    const filesToDelete = arrayDiff(currentFiles, filesUpdated);

    if (filesToDelete.length > 0) {
      log('Removing files...');
      await deleteFilesDSFS(config, filesToDelete, config.rootPath);
      log('Files removed');
    }

    process.exit(0);

  } catch (error) {
    logError('Failed to push files');
    logError(error);
    process.exit(1);
  }

};


