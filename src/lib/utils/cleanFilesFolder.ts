import fs from 'fs';
import { DS_FOLDERS, CURRENT_DIR } from '../../constants';
import { DSBundleConfig } from './config';

export const cleanFilesFolder = (config: DSBundleConfig) => {
  if (config.isAtRoot) {
    DS_FOLDERS.forEach((dir) => {
      try {
        if (fs.existsSync(`./${dir}`)) {
          fs.rmSync(`./${dir}`, { recursive: true });
        }
      } catch (error) {
        console.error(error);
      }
    });
    return;
  }

  cleanFilesSubFolder(`${config.rootPath}${config.targetPathRelativeToRoot}`);
};

export const cleanFilesSubFolder = (folder: string) => {
  try {
    if (fs.existsSync(folder)) {
      const files = fs.readdirSync(folder)
      files.forEach((file) => {
        fs.rmSync(file, { recursive: true });
      });
    }
  } catch (error) {
    console.error(error);
  }
};
