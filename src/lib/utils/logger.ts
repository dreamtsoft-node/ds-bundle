import chalk from 'chalk';

const logger = console.log;
const OBJECT_DEBUG_PREPEND = 'Object => ';

export function log(data) {
  if (typeof data === 'string') {
    logger(timestampPrepend(data));
    return;
  }
  logger(timestampPrepend(OBJECT_DEBUG_PREPEND), data);
}

export function logError(data, data2?) {
  const timestamp = timestampPrepend();

  if (typeof data === 'string') {
    logger(timestamp + errorPrepend(data));
    if (data2) {
      logger(timestamp + errorPrepend(OBJECT_DEBUG_PREPEND), data2);
    }
    return;
  }

  logger(timestamp + errorPrepend(OBJECT_DEBUG_PREPEND), data);
  if (data2) {
    logger(timestamp + errorPrepend(OBJECT_DEBUG_PREPEND), data2);
  }
}

function errorPrepend(message = '') {
  return chalk.red(`Error ${message}`);
}

function timestampPrepend(message = '') {
  const date = new Date();
  return chalk.blue(`[${date.toLocaleTimeString()}]`) + ' ' + message;
}
