import { CONFIG_FILE_NAME, DS_AUTH_FILE_NAME, CURRENT_DIR } from '../../constants';
import { readFileSync, existsSync } from 'fs';
import ignore from 'ignore';
import path from 'path';

export interface DSBundleConfig {
    space: string,
    bundle: string,
    baseUrl: string,
    auth: {
        username: string,
        password: string,
    },
    rootPath: string,
    isAtRoot: boolean,
    targetPathRelativeToRoot: string;
    shouldIgnorePath: (ignorePath: string) => boolean,
}

export const loadConfig = (): DSBundleConfig | undefined => {
    const rootPath = findConfigRoot(CURRENT_DIR);

    if (!rootPath) {
      return;
    }

    const isAtRoot = rootPath === CURRENT_DIR;
    const targetPathRelativeToRoot = isAtRoot ? '' : CURRENT_DIR.replace(rootPath, '').slice(0, -1);

    const config = readFileSync(`${rootPath}${CONFIG_FILE_NAME}`, 'utf-8');
    const newConfig = JSON.parse(config);

    let auth = readFileSync(`${rootPath}${DS_AUTH_FILE_NAME}`, 'utf-8');
    auth = JSON.parse(auth);

    const finalConfig = Object.assign({}, newConfig, { auth, rootPath, targetPathRelativeToRoot, isAtRoot, shouldIgnorePath: createShouldIgnorePath(newConfig.ignore) });
    return finalConfig as DSBundleConfig;
};

function createShouldIgnorePath(paths?: string[]) {
  const ignorePaths = paths && Array.isArray(paths) ? paths : [];
  const ig = ignore().add(ignorePaths);

  return (ignorePath: string) => {
    return ig.ignores(ignorePath)
  };
}

const findConfigRoot = (startPath: string): string | undefined => {
  if (existsSync(`${startPath}${CONFIG_FILE_NAME}`)) {
    return startPath;
  }

  let parentPath = path.dirname(startPath);

  if (parentPath === '/') {
    return;
  }

  parentPath += '/';

  const possibleConfigFile = `${parentPath}${CONFIG_FILE_NAME}`;

  if (existsSync(possibleConfigFile)) {
    return parentPath;
  }

  return findConfigRoot(parentPath);
};

export const cleanTargetPath = (targetPath): string => {
  const last = targetPath.charAt(targetPath.length - 1);

  if (last == '/') {
    return targetPath.slice(0, -1);
  }

  return targetPath;
}
