
export const arrayDiff = (files1, files2) => {
  return files1.filter(x => files2.indexOf(x) === -1);
}
