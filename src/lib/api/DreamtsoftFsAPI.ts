const axios = require('axios');
import * as path from 'path';
import fs from 'fs';

export class DreamtsoftFsAPI {

  baseUrl: string;
  spaceId: string;
  bundleId: string;
  debug: boolean;
  httpClient: any;

  constructor(baseUrl, spaceId, username, password, bundleId) {
    this.baseUrl = baseUrl;
    this.spaceId = spaceId;
    this.bundleId = bundleId;
    this.debug = false;
    this.setup(username, password);
  }

  setDebug(flag) {
    this.debug = flag;
  }

  setBundleId(bundleId) {
    this.bundleId = bundleId;
  }

  setup(username, password) {
    const baseURL = !this.spaceId
      ? `${this.baseUrl}/api/ds.base/` : `${this.baseUrl}/s/${this.spaceId}/api/ds.base/`;

    const credentials = Buffer.from(username + ':' + password).toString('base64');
    const basicAuth = 'Basic ' + credentials;

    this.httpClient = axios.create({
      baseURL,
      timeout: 1000 * 60 * 5,
      headers: {
        'Authorization': basicAuth,
        'Content-Type': 'application/json',
      },
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
    });

    this.httpClient.interceptors.request.use(
      (req) => {
        if (this.debug) {
          console.log('-------- Request DreamtsoftFsAPI ---------');
          console.log(`${req.baseURL}${req.url}`);
          console.log(JSON.stringify(req.headers));
          console.log(JSON.stringify(req.data));
        }

        return req;
      },
      (error) => Promise.reject(error)
    );
  }

  async request(params) {
    let response;
    try {
      params.bundleId = this.bundleId;
      response = await this.httpClient.post(`dreamtsoftFsAPI`, params);
    } catch (error: any) {
      if (!error.response) {
        throw error;
      } else {
        response = error.response;
      }

    }

    const { status, statusText, data } = response;

    if (this.debug && response.status !== 404 && response.status !== 200) {
      console.log('-------- Bad response ---------');
      console.log('-------- Request Debug ---------');
      console.log('Request params: ', JSON.stringify(params));
      console.log('-------- Response Debug ---------');
      console.log('Response status: ', status);
      console.log('Response statusText: ', statusText);
      console.log('Response data: ', data);
    }

    if (status === 404) {
      if (data.errorMsg) {
        console.warn(data.errorMsg);
      }
      return;
    } else if (status !== 200) {
      if (data.errorMsg) {
        console.error(data.errorMsg);
        throw Error(`${status} - ${data.errorMsg}`);
      }

      if (status === 401) {
        throw Error(`Invalid credentials`);
      }

      throw Error(`${status} Error`);
    }

    return data;
  }

  pathTransform(dsPath) {
    return `${this.bundleId}${dsPath}`;
  }

  async readDirectory(dsPath) {
    const files = await this.request({
      command: 'readDirectory',
      path: this.pathTransform(dsPath),
    });
    return files;
  }

  async readFile(dsPath) {
    const file = await this.request({
      command: 'readFile',
      path: this.pathTransform(dsPath),
    });

    return Buffer.from(file.content);
  }

  async writeFile(dsPath, content: string, create?: boolean) {
    const dirname = `${this.bundleId}${path.dirname(dsPath)}`;

    const file = await this.request({
      command: 'writeFile',
      path: this.pathTransform(dsPath),
      content: content.toString(),
      name: path.basename(dsPath),
      dirname: dirname,
      create,
    });

    if (!create && file === undefined) {
     console.error('Could not update file: ' + dsPath);
    }

  }

  async createDirectory(dsPath) {
    const dirname = `${this.bundleId}/${path.dirname(dsPath)}`;
    await this.request({
        command: 'createDirectory',
        path: this.pathTransform(dsPath),
        name: path.basename(dsPath),
        dirname: dirname,
    });
  }

  async delete(dsPath) {
    const result = await this.request({
      command: 'delete',
      path: this.pathTransform(dsPath),
    });

    if (result === undefined) {
      console.error('Could not delete file: ' + dsPath);
    }
  }

  async deleteFiles(files: IDSFile[]) {

    let filesPayload = files.map((file) => {
      const fPath = this.pathTransform(file.path);
      return {
        path: fPath,
      };

    });

    return await this.request({
      command: 'deleteFiles',
      files: filesPayload,
    });

  }

  async updateFileOrCreate(dsPath, content) {
    const dirname = `${this.bundleId}${path.dirname(dsPath)}`;

    const file = await this.request({
      command: 'updateFileOrCreate',
      path: this.pathTransform(dsPath),
      content: content.toString(),
      name: path.basename(dsPath),
      dirname: dirname,
    });
  }

  async updateFilesOrCreate(files: IDSFile[]) {

    let filesPayload = files.map((file) => {
      const name = path.basename(file.path);
      const fPath = this.pathTransform(file.path);
      const dirname = path.dirname(fPath);

      return {
        dirname,
        name,
        path: fPath,
        content: file.content,
        isDirectory: file.isDirectory,
      };

    });

    return await this.request({
      command: 'updateFilesOrCreate',
      files: filesPayload,
    });

  }

}


export interface IDSFile {
  path: string;
  content?: string;
  isDirectory?: boolean;
}
