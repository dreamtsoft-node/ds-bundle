import { DreamtsoftFsAPI } from '../api/DreamtsoftFsAPI';
const fs = require('fs');
const path = require('path');
import { DS_FOLDERS, BATCH_SIZE_FOR_WRITING_FILES } from '../../constants';
import { log, logError } from '../../lib/utils/logger';

/**
 * Watch files
 */

 export async function watchFiles(config, watcher, bundlesFolderPath) {
  const dreamtsoftFsAPI = new DreamtsoftFsAPI(
      config.baseUrl,
      config.space,
      config.auth.username,
      config.auth.password,
      config.bundle
  );

  return await watchFolders(dreamtsoftFsAPI, watcher, bundlesFolderPath, config.shouldIgnorePath);
}


function watchFolders(dreamtsoftFsAPI, watcher, bundlesFolderPath, shouldIgnorePath) {
  return new Promise((accept, reject) => {
    watcher
    .on('add', fpath => createFile(fpath, dreamtsoftFsAPI, bundlesFolderPath, shouldIgnorePath))
    .on('change', fpath => updateFile(fpath, dreamtsoftFsAPI, bundlesFolderPath, shouldIgnorePath))
    .on('unlink', fpath => deleteFileOrDir(fpath, dreamtsoftFsAPI, bundlesFolderPath, shouldIgnorePath))
    .on('addDir', fpath => createDir(fpath, dreamtsoftFsAPI, bundlesFolderPath, shouldIgnorePath))
    .on('unlinkDir', fpath => deleteFileOrDir(fpath, dreamtsoftFsAPI, bundlesFolderPath, shouldIgnorePath))
    .on('error', error => {
      logError(`Watcher error: ${error}`)
    })
    .on('ready', () => {
      accept(undefined);
    });
  });
}

async function createFile(fpath: string, dreamtsoftFsAPI, bundlesFolderPath: string, shouldIgnorePath) {
  try {
    const ignorePath = getIgnorePath(fpath, bundlesFolderPath);
    if (shouldIgnorePath(ignorePath)) {
      return;
    }

    const dsPath = getDsPath(fpath, bundlesFolderPath);
    const content = fs.readFileSync(fpath, 'utf-8');
    log(`Creating.. ${dsPath}`);
    await dreamtsoftFsAPI.writeFile(dsPath, content, true);
    log(`File created: ${dsPath}`);
  } catch (error) {
    logError(`Failed to create: ${fpath}`);
    logError(error);
  }
}

async function createDir(fpath: string, dreamtsoftFsAPI, bundlesFolderPath: string, shouldIgnorePath) {
  try {
    const ignorePath = getIgnorePath(fpath, bundlesFolderPath);
    if (shouldIgnorePath(ignorePath)) {
      return;
    }

    const dsPath = getDsPath(fpath, bundlesFolderPath);
    log(`Creating.. ${dsPath}`);
    await dreamtsoftFsAPI.createDirectory(dsPath);
    log(`Directory created: ${dsPath}`);
  } catch (error) {
    logError(`Failed to create: ${fpath}`);
    logError(error);
  }
}

async function updateFile(fpath: string, dreamtsoftFsAPI, bundlesFolderPath: string, shouldIgnorePath) {
  try {
    const ignorePath = getIgnorePath(fpath, bundlesFolderPath);
    if (shouldIgnorePath(ignorePath)) {
      return;
    }

    const dsPath = getDsPath(fpath, bundlesFolderPath);
    const content = fs.readFileSync(fpath, 'utf-8');
    log(`Updating.. ${dsPath}`);
    await dreamtsoftFsAPI.writeFile(dsPath, content);
    log(`File updated: ${dsPath}`);
  } catch (error) {
    logError(`Failed to update: ${fpath}`);
    logError(error);
  }
}

async function deleteFileOrDir(fpath: string, dreamtsoftFsAPI, bundlesFolderPath: string, shouldIgnorePath) {
  try {
    const ignorePath = getIgnorePath(fpath, bundlesFolderPath);
    if (shouldIgnorePath(ignorePath)) {
      return;
    }

    const dsPath = getDsPath(fpath, bundlesFolderPath);
    log(`Deleting.. ${dsPath}`);
    await dreamtsoftFsAPI.delete(dsPath);
    log(`File removed: ${dsPath}`);
  } catch (error) {
    logError(`Failed to delete: ${fpath}`);
    logError(error);
  }
}

function getDsPath(p: string, bundlesFolderPath) {
  return p.replace(bundlesFolderPath, '/');
}

function getIgnorePath(p: string, bundlesFolderPath) {
  return p.replace(bundlesFolderPath, '');
}

function tryPromiseInMs(msTime, promiseFn) {
  return new Promise((accept, reject) => {
    setTimeout(() => {
      promiseFn.then(accept)
      .catch(reject);
    }, msTime);
  });
}
