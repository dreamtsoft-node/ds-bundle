import { DreamtsoftFsAPI } from '../api/DreamtsoftFsAPI';
const fs = require('fs');
const path = require('path');
import { DS_FOLDERS, BATCH_SIZE_FOR_WRITING_FILES, BATCH_SIZE_FOR_DELETING_FILES } from '../../constants';
import { log, logError } from '../../lib/utils/logger';

/**
 * Write files supporting functions
 */

 export async function writeFilesDSFS(config, bundlesFolderPath, targetPath?) {
  const dreamtsoftFsAPI = new DreamtsoftFsAPI(
      config.baseUrl,
      config.space,
      config.auth.username,
      config.auth.password,
      config.bundle
  );

  const filesTouched = [];

  await writeFiles(dreamtsoftFsAPI, `${bundlesFolderPath}`, config.shouldIgnorePath, targetPath, filesTouched);

  return filesTouched;
}

 async function writeFiles(dreamtsoftFsAPI, bundlesFolderPath, shouldIgnorePath, targetPath = '', filesTouched) {
  const filesQueue: any[] = [];
  const foldersToLookIn = targetPath ? DS_FOLDERS.filter((f) => targetPath.startsWith(f)) : DS_FOLDERS;

  foldersToLookIn.forEach((dir) => {
    const startPath = targetPath ? bundlesFolderPath + targetPath : bundlesFolderPath + dir;

    const files = getAllLocalFilesForDir(startPath, shouldIgnorePath, bundlesFolderPath);
    if (!files) {
      return;
    }
    sliceIntoChunks(files, BATCH_SIZE_FOR_WRITING_FILES).forEach((chunk) => {
      filesQueue.push(chunk);
    });
  });

  return new Promise((accept, reject) => {
    let intervalId;
    const intervalMs = 100;
    let uploadLocked = false;

    // Upload files in chunks
    intervalId = setInterval(() => {
      if (uploadLocked) {
        return;
      }
      uploadLocked = true;

      if (filesQueue.length === 0) {
        clearInterval(intervalId);
        accept(undefined);
        uploadLocked = false;
        return;
      }

      let chunkOfFiles: any[] | undefined = filesQueue.shift();

      if (chunkOfFiles === undefined || (Array.isArray(chunkOfFiles) && chunkOfFiles.length === 0)) {
        uploadLocked = false;
        return;
      }

      /**
       * Get file content
       */

      chunkOfFiles = chunkOfFiles.map((filePath: string) => {
        const dsPath = filePath.replace(bundlesFolderPath, '/');

        if (fs.statSync(filePath).isDirectory()) {
          return {
            isDirectory: true,
            path: dsPath,
          };
        }

        const f = {
          content: null,
          path: dsPath,
        };

        try {
          f.content = fs.readFileSync(filePath, 'utf-8');
        } catch (error) {
          clearInterval(intervalId);
          reject(error);
          return
        }

        return f;
      });

      dreamtsoftFsAPI.updateFilesOrCreate(chunkOfFiles)
        .then(({ errors }) => {
          if (Array.isArray(errors) && errors.length > 0) {
            let errs = 'Issue uploading the following files';
            errs += errors.map((errr) => {
              return `\n${errr}`;
            }).join('');

            clearInterval(intervalId);
            reject(errs);
            return;
          }

          chunkOfFiles?.forEach((data) => {
            filesTouched.push(data.path);
            log(data.path);
          });

          uploadLocked = false;
        })
        .catch((err) => {
          clearInterval(intervalId);
          reject(err);
        });

    }, intervalMs);
  });

}

function getAllLocalFilesForDir(dirPath, shouldIgnorePath, bundlesFolderPath, arrayOfFiles?) {
  if (!fs.existsSync(dirPath)) {
    return;
  }
  const files = fs.readdirSync(dirPath)

  arrayOfFiles = arrayOfFiles || []

  files.forEach((file) => {
    const fullPathFile = `${dirPath}/${file}`;
    const ignorePath = fullPathFile.replace(bundlesFolderPath, '');

    if (shouldIgnorePath(ignorePath)) {
      return;
    }

    if (fs.statSync(fullPathFile).isDirectory()) {
      arrayOfFiles.push(fullPathFile);
      arrayOfFiles = getAllLocalFilesForDir(fullPathFile, shouldIgnorePath, bundlesFolderPath, arrayOfFiles);
    } else {
      arrayOfFiles.push(fullPathFile);
    }
  });

  return arrayOfFiles
}

function sliceIntoChunks(arr, chunkSize) {
  const res: any[] = [];
  for (let i = 0; i < arr.length; i += chunkSize) {
      const chunk = arr.slice(i, i + chunkSize);
      res.push(chunk);
  }
  return res;
}

export async function deleteFilesDSFS(config, files, bundlesFolderPath) {
  const dreamtsoftFsAPI = new DreamtsoftFsAPI(
      config.baseUrl,
      config.space,
      config.auth.username,
      config.auth.password,
      config.bundle
  );
  await deleteFiles(dreamtsoftFsAPI, files, bundlesFolderPath);
}

async function deleteFiles(dreamtsoftFsAPI, files, bundlesFolderPath) {
  const filesQueue: any[] = [];

  sliceIntoChunks(files, BATCH_SIZE_FOR_DELETING_FILES).forEach((chunk) => {
    filesQueue.push(chunk);
  });

  return new Promise((accept, reject) => {
    let intervalId;
    const intervalMs = 100;
    let uploadLocked = false;

    // Upload files in chunks
    intervalId = setInterval(() => {
      if (uploadLocked) {
        return;
      }
      uploadLocked = true;

      if (filesQueue.length === 0) {
        clearInterval(intervalId);
        accept(undefined);
        uploadLocked = false;
        return;
      }

      let chunkOfFiles: any[] | undefined = filesQueue.shift();

      if (chunkOfFiles === undefined || (Array.isArray(chunkOfFiles) && chunkOfFiles.length === 0)) {
        uploadLocked = false;
        return;
      }

      /**
       * Fix paths to dspath
       */

      chunkOfFiles = chunkOfFiles.map((filePath: string) => {
        const dsPath = filePath.replace(bundlesFolderPath, '/');
        return {
          path: dsPath,
        };

      });

      dreamtsoftFsAPI.deleteFiles(chunkOfFiles)
        .then(({ errors }) => {
          if (Array.isArray(errors) && errors.length > 0) {
            let errs = 'Issue uploading the following files';
            errs += errors.map((errr) => {
              return `\n${errr}`;
            }).join('');

            clearInterval(intervalId);
            reject(errs);
            return;
          }

          chunkOfFiles?.forEach((data) => {
            log(data.path);
          });

          uploadLocked = false;
        })
        .catch((err) => {
          clearInterval(intervalId);
          reject(err);
        });


    }, intervalMs);
  });

}
