import { DreamtsoftFsAPI } from '../api/DreamtsoftFsAPI';
const fs = require('fs');
const path = require('path');
import { DS_FOLDERS, BATCH_SIZE_FOR_WRITING_FILES } from '../../constants';
import { log, logError } from '../../lib/utils/logger';


export async function readFilesDSFS(config, bundlesFolderPath, targetPath?) {
  const dreamtsoftFsAPI = new DreamtsoftFsAPI(
      config.baseUrl,
      config.space,
      config.auth.username,
      config.auth.password,
      config.bundle
  );
  await readFiles(dreamtsoftFsAPI, `${bundlesFolderPath}`, targetPath);
}

async function readFiles(dreamtsoftFsAPI, bundlesFolderPath, targetPath?) {
    const root =  await dreamtsoftFsAPI.readDirectory(targetPath ? `/${targetPath}` : '/');
    await crawlDirAndReadFiles(dreamtsoftFsAPI, root, bundlesFolderPath);
}

async function crawlDirAndReadFiles(dreamtsoftFsAPI, dirsOrFilesList, bundlesFolderPath) {
    const dirsTypes = ['locked', 'system', 'folder'];
    const files: any[] = [];
    const dirs: any[] = [];

    for (let index = 0; index < dirsOrFilesList.length; index++) {
        const data = dirsOrFilesList[index];

        if (data.type === 'file' && !data.path.startsWith('/bucket')) {
            const fileContents = await dreamtsoftFsAPI.readFile(data.path);
            files.push(Object.assign({ fileContents: fileContents }, data));

        } else if (dirsTypes.includes(data.type) && !data.path.startsWith('/bucket')) {
            const dirContents = await dreamtsoftFsAPI.readDirectory(data.path);
            dirs.push({ path: data.path, contents: dirContents.slice(0) });

            ensureDirectoryExistence(`${bundlesFolderPath}${data.path}`);
        }
    }

    if (files.length > 0) {
      writeLocalFiles(files, bundlesFolderPath);
    }

    for (let index = 0; index < dirs.length; index++) {
        const dirData = dirs[index];
        await crawlDirAndReadFiles(dreamtsoftFsAPI, dirData.contents, bundlesFolderPath);
    }
}

function writeLocalFiles(files, bundlesFolderPath) {
    files.forEach((file) => {
        ensureDirectoryExistence(`${bundlesFolderPath}${file.path}`);
        fs.writeFileSync(`${bundlesFolderPath}${file.path}`, file.fileContents);
        log(file.path);
    });
}

function ensureDirectoryExistence(filePath) {
    const dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
        return true;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
}


export async function listFilesDSFS(config, bundlesFolderPath, targetPath?) {
  const dreamtsoftFsAPI = new DreamtsoftFsAPI(
      config.baseUrl,
      config.space,
      config.auth.username,
      config.auth.password,
      config.bundle
  );
  return await listFiles(dreamtsoftFsAPI, `${bundlesFolderPath}`, targetPath);
}

async function listFiles(dreamtsoftFsAPI, bundlesFolderPath, targetPath?) {
    const root =  await dreamtsoftFsAPI.readDirectory(targetPath ? `/${targetPath}` : '/');
    const files = [];
    await crawlDirAndListFiles(dreamtsoftFsAPI, root, bundlesFolderPath, files);
    return files;
}

async function crawlDirAndListFiles(dreamtsoftFsAPI, dirsOrFilesList, bundlesFolderPath, allFiles) {
    const dirsTypes = ['locked', 'system', 'folder'];
    const dirsTypesThatCanBeDeleted = ['folder']; // only list folders that can be deleted
    const files: any[] = [];
    const dirs: any[] = [];

    for (let index = 0; index < dirsOrFilesList.length; index++) {
        const data = dirsOrFilesList[index];

        if (data.type === 'file' && !data.path.startsWith('/bucket')) {
            // files to top of list
            files.unshift(Object.assign({}, data));

        } else if (dirsTypes.includes(data.type) && !data.path.startsWith('/bucket')) {
            const dirContents = await dreamtsoftFsAPI.readDirectory(data.path);
            dirs.push({ path: data.path, contents: dirContents.slice(0) });
            if (dirsTypesThatCanBeDeleted.includes(data.type)) {
              // folders to bottom of list
              files.push(Object.assign({}, data));
            }

        }
    }

    if (files.length > 0) {
      files.forEach((file) => {
        if (dirsTypesThatCanBeDeleted.includes(file.type)) {
          // folders to bottom of list
          allFiles.push(file.path);
        } else {
           // files to top of list
          allFiles.unshift(file.path);
        }
      });
    }

    for (let index = 0; index < dirs.length; index++) {
        const dirData = dirs[index];
        await crawlDirAndListFiles(dreamtsoftFsAPI, dirData.contents, bundlesFolderPath, allFiles);
    }
}
