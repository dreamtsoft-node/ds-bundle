
export const CONFIG_FILE_NAME = 'ds-bundle.json';
export const DS_AUTH_FILE_NAME = '.ds-bundle-auth.json';

export const DS_FOLDERS = [
  "components",
  "actions",
  "auth",
  "widget",
  "form_extension",
  "bundle_components",
  // "bucket", // lets no manage bucket since they can be quite large
  "content",
  "modules",
  "tests"
];

export const CURRENT_DIR =  process.cwd() + '/';

export const BATCH_SIZE_FOR_WRITING_FILES = 5;
export const BATCH_SIZE_FOR_DELETING_FILES = 5;
